<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\EventDispatcher;

use MarioDevment\Performance\Infrastructure\RabbitMQ\RabbitMQPublisher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class ImageSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            Event::whenTheImageUpload() => [
                [
                    'publishRabbitMQSaveDatabase',
                    10,
                ],
                [
                    'publishRabbitMQResizeImage',
                ],
                [
                    'publishRabbitMQFilterImage',
                ],
            ],
            Event::whenImageChanged()   => [
                [
                    'publishRabbitMQSaveDatabase',
                ],
            ],
        ];
    }

    public function publishRabbitMQResizeImage(RabbitMQPublisher $publisher)
    {
        $publisher->queueToResizeOnRabbitMQ();
    }

    public function publishRabbitMQSaveDatabase(RabbitMQPublisher $publisher)
    {
        $publisher->queueSaveToDatabaseOnRabbitMQ();
    }

    public function publishRabbitMQFilterImage(RabbitMQPublisher $publisher)
    {
        $publisher->queueToFilterOnRabbitMQ();
    }
}
