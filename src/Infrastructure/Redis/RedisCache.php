<?php

namespace MarioDevment\Performance\Infrastructure\Redis;

use Predis\Client;

class RedisCache
{
    private $redis;

    public function __construct(Client $redis)
    {
        $this->redis = $redis;
    }

    public function get(string $key): ?string
    {
        return $this->redis->get($key);
    }

    public function invalidate(string $key): void
    {
        $this->redis->del([$key]);
    }

    public function set(string $key, string $value, int $ttl = 0): void
    {
        if ($ttl > 0) {
            $this->redis->setex($key, $ttl, $value);
        } else {
            $this->redis->set($key, $value);
        }
    }
}
