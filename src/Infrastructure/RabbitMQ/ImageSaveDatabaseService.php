<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\RabbitMQ;

use Doctrine\ORM\EntityManager;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageEntry;
use MarioDevment\Performance\Infrastructure\Redis\RedisCache;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Predis\Client;

final class ImageSaveDatabaseService implements ConsumerInterface
{
    private $entityManager;
    private $redis;
    private const KEY = 'list_image';

    public function __construct(EntityManager $entityManager, Client $redis)
    {
        $this->entityManager = $entityManager;
        $this->redis         = $redis;
    }

    public function execute(AMQPMessage $msg)
    {
        $response = json_decode($msg->body, true);

        $imageEntry = ImageEntry::jsonDeserialize($response);

        $this->entityManager->persist($imageEntry);
        $this->entityManager->flush();

        $this->persistOnRedis($imageEntry);
    }

    private function persistOnRedis(ImageEntry $imageEntry): void
    {
        $redisCache = new RedisCache($this->redis);

        $id        = $imageEntry->id();
        $imageJson = json_encode($imageEntry->jsonSerialize());

        $redisCache->set((string) $id, $imageJson);
        $redisCache->invalidate(self::KEY);

    }
}
