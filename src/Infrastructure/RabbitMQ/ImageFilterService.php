<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\RabbitMQ;

use Gumlet\ImageResize;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageEntry;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageName;
use MarioDevment\Performance\Infrastructure\EventDispatcher\Event;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\KernelInterface;

final class ImageFilterService implements ConsumerInterface
{
    private const UPLOAD_IMAGES_DIRECTORY = '/upload/images/';
    private const PUBLIC_DIRECTORY        = '/public/';
    private const GRAY_FILTER             = 'gray';
    private $kernel;
    private $eventDispatcher;

    public function __construct(KernelInterface $kernel, EventDispatcherInterface $eventDispatcher)
    {
        $this->kernel          = $kernel;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function execute(AMQPMessage $msg)
    {
        $imageEntry    = $this->deserialize($msg);
        $publicDir     = $this->publicDirectory();
        $imageToFilter = $this->instanceImage($publicDir, $imageEntry);

        $typeFilter = $this->filterImage($imageEntry, $imageToFilter);

        $this->changeName($typeFilter, $imageEntry);

        $savePath = $this->path($publicDir, $imageEntry);
        $imageToFilter->save($savePath);
        $imageEntry->addTag($typeFilter);
        $this->notifyChangeImageToRabbitMQ($imageEntry);
    }

    private function imagePath(string $localPath, ImageEntry $imageEntry): string
    {
        $imagePath = $localPath . $imageEntry->path()->value() . $imageEntry->name()->value() . '.' . $imageEntry->ext()->value();

        return $imagePath;
    }

    private function path(string $localPath, ImageEntry $imageEntry): string
    {
        $path = $localPath . self::UPLOAD_IMAGES_DIRECTORY . $imageEntry->name()->value() . '.' . $imageEntry->ext()->value();

        return $path;
    }

    private function changeName(string $typeFilter, ImageEntry $imageEntry): void
    {
        $newName    = '[' . $typeFilter . ']' . $imageEntry->name()->value();

        $scaledName = new ImageName($newName);
        $imageEntry->changeName($scaledName);
    }

    private function publicDirectory(): string
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $publicDir = $this->kernel->getProjectDir() . self::PUBLIC_DIRECTORY;

        return $publicDir;
    }

    private function deserialize(AMQPMessage $msg): ImageEntry
    {
        $response   = json_decode($msg->body, true);
        $imageEntry = ImageEntry::jsonDeserialize($response);

        return $imageEntry;
    }

    private function instanceImage(string $publicDir, ImageEntry $imageEntry): ImageResize
    {
        $imagePath   = $this->imagePath($publicDir, $imageEntry);
        $imageFilter = new ImageResize($imagePath);

        return $imageFilter;
    }

    private function addFilterGray(ImageResize $imageToFilter): void
    {
        $imageToFilter->addFilter(
            function ($imageDesc) {
                imagefilter($imageDesc, IMG_FILTER_GRAYSCALE);
            }
        );
    }

    private function addFilterNegate(ImageResize $imageToFilter): void
    {
        $imageToFilter->addFilter(
            function ($imageDesc) {
                imagefilter($imageDesc, IMG_FILTER_NEGATE);
            }
        );
    }

    private function filterImage(ImageEntry $imageEntry, ImageResize $imageToFilter): string
    {
        $typeFilter = $imageEntry->filter()->value();

        if ($typeFilter == self::GRAY_FILTER) {
            $this->addFilterGray($imageToFilter);
        } else {
            $this->addFilterNegate($imageToFilter);
        }

        return $typeFilter;
    }

    private function notifyChangeImageToRabbitMQ(ImageEntry $imageEntry): void
    {
        $container         = $this->kernel->getContainer();
        $publishToRabbitMQ = new RabbitMQPublisher($container, $imageEntry);

        $this->eventDispatcher->dispatch(
            Event::whenImageChanged(),
            $publishToRabbitMQ
        );
    }
}
