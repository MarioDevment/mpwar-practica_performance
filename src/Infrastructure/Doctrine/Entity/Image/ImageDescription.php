<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image;

final class ImageDescription
{
    private $description;

    public function __construct(string $description = '')
    {
        $this->description = $description;
    }

    public function value(): string
    {
        return $this->description;
    }
}
