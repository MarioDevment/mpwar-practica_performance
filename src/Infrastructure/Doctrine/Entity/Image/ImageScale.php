<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image;

final class ImageScale
{
    private $scale;

    public function __construct(int $scale)
    {
        $this->scale = $scale;
    }

    public function value(): int
    {
        return $this->scale;
    }
}
