<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageClient;

final class Client extends Type
{
    const NAME = 'client';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ImageClient
    {
        $client = new ImageClient($value);
        return $client;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        return $value->value();
    }

    public function getName()
    {
        return static::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
