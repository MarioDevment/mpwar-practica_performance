<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageTags;

final class Tags extends Type
{
    const NAME = 'tags';

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ImageTags
    {
        $tags = new ImageTags(explode(',', $value));
        return $tags;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        return implode(',', $value->value());
    }

    public function getName()
    {
        return static::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
