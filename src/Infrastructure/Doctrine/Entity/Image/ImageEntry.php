<?php
declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;

/**
 * @ORM\Entity(repositoryClass="MarioDevment\Performance\Infrastructure\Doctrine\Repository\ImageRepository")
 * @ORM\Table(name="image")
 */
final class ImageEntry
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @Column(type="uuid", columnDefinition="CHAR(40) NOT NULL")
     *
     * @var ImageUuid
     */
    private $uuid;
    /**
     * @Column(type="name", columnDefinition="CHAR(255) NOT NULL")
     *
     * @var ImageName
     */
    private $name;
    /**
     * @Column(type="client", columnDefinition="CHAR(255) NOT NULL")
     *
     * @var ImageName
     */
    private $client;
    /**
     * @Column(type="ext", columnDefinition="CHAR(255) NOT NULL")
     *
     * @var ImageName
     */
    private $ext;
    /**
     * @Column(type="scale", columnDefinition="CHAR(255) NOT NULL")
     *
     * @var ImageScale
     */
    private $scale;
    /**
     * @Column(type="filter", columnDefinition="CHAR(255) NOT NULL")
     *
     * @var ImageFilter
     */
    private $filter;
    /**
     * @Column(type="tags", columnDefinition="CHAR(255) NOT NULL")
     *
     * @var ImageTags
     */
    private $tags;
    /**
     * @Column(type="path", columnDefinition="CHAR(255) NOT NULL")
     *
     * @var ImagePath
     */
    private $path;
    /**
     * @Column(type="description", columnDefinition="CHAR(255) NOT NULL")
     *
     * @var ImageDescription
     */
    private $description;

    public function __construct(
        ImageUuid $uuid,
        ImageName $name,
        ImageClient $client,
        ImageExtension $extension,
        ImagePath $path,
        ImageScale $scale,
        ImageFilter $filter,
        ImageTags $tags,
        ImageDescription $description
    ) {
        $this->uuid        = $uuid;
        $this->name        = $name;
        $this->path        = $path;
        $this->scale       = $scale;
        $this->filter      = $filter;
        $this->tags        = $tags;
        $this->client      = $client;
        $this->ext         = $extension;
        $this->description = $description;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function uuid(): ImageUuid
    {
        return $this->uuid;
    }

    public function name(): ImageName
    {
        return $this->name;
    }

    public function path(): ImagePath
    {
        return $this->path;
    }

    public function scale(): ImageScale
    {
        return $this->scale;
    }

    public function filter(): ImageFilter
    {
        return $this->filter;
    }

    public function tags(): ImageTags
    {
        return $this->tags;
    }

    public function description(): ImageDescription
    {
        return $this->description;
    }

    public function client(): ImageClient
    {
        return $this->client;
    }

    public function ext(): ImageExtension
    {
        return $this->ext;
    }

    public function jsonSerialize(): array
    {
        return [
            'id'          => $this->id,
            'uuid'        => $this->uuid()->value(),
            'name'        => $this->name()->value(),
            'client'      => $this->client()->value(),
            'ext'         => $this->ext()->value(),
            'path'        => $this->path()->value(),
            'scale'       => $this->scale()->value(),
            'filter'      => $this->filter()->value(),
            'tags'        => implode(',', $this->tags->value()),
            'description' => $this->description()->value(),
        ];
    }

    public static function jsonDeserialize(array $imageRaw): ImageEntry
    {
        $uuid        = new ImageUuid($imageRaw['uuid']);
        $name        = new ImageName($imageRaw['name']);
        $client      = new ImageClient($imageRaw['client']);
        $ext         = new ImageExtension($imageRaw['ext']);
        $path        = new ImagePath($imageRaw['path']);
        $scala       = new ImageScale($imageRaw['scale']);
        $filter      = new ImageFilter($imageRaw['filter']);
        $tags        = new ImageTags(explode(',', $imageRaw['tags']));
        $description = new ImageDescription($imageRaw['description']);

        $imageEntry = new ImageEntry($uuid, $name, $client, $ext, $path, $scala, $filter, $tags, $description);

        return $imageEntry;
    }

    public function changeScala(ImageScale $scale): void
    {
        $this->scale = $scale;
    }

    public function changeFilter(ImageFilter $filter): void
    {
        $this->filter = $filter;
    }

    public function addTag(string $tag): void
    {
        $this->tags->addTag($tag);
    }

    public function changeName(ImageName $name): void
    {
        $this->name = $name;
    }

    public function setRedisId(int $id): void
    {
        $this->id = $id;
    }

    public function setDescription(ImageDescription $description): void
    {
        $this->description = $description;
    }

    public function setTags(ImageTags $tags): void
    {
        $this->tags = $tags;
    }
}
