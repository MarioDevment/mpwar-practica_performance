<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image;

final class ImageClient
{
    private $client;

    public function __construct(string $client)
    {
        $this->client = $client;
    }

    public function value(): string
    {
        return $this->client;
    }
}
