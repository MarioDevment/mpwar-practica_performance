<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Infrastructure\Doctrine\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageDescription;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageEntry;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageTags;
use PDO;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ImageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ImageEntry::class);
    }

    public function findAllUuid(): array
    {
        $qb = $this->createQueryBuilder('i')
            ->select('i.uuid')
            ->getQuery();

        return $qb->execute();
    }

    public function updateTagDescription(string $tag, string $description, string $uuid, string $name): string
    {
        $id = $this->getIdUpdate($uuid, $name);

        $em    = $this->getEntityManager();
        $image = $em->getRepository(ImageEntry::class)->find($id);

        $tags         = new ImageTags(explode(',', $tag));
        $descriptions = new ImageDescription($description);
        $image->setTags($tags);
        $image->setDescription($descriptions);

        $em->flush();

        return $id;
    }

    public function findAllId(): array
    {
        $qb = $this->createQueryBuilder('i')
            ->select('i.id')
            ->getQuery();

        return $qb->execute();
    }

    public function findById($id)
    {
        $qb = $this->createQueryBuilder('i')
            ->select(
                'i.uuid',
                'i.filter',
                'i.name',
                'i.path',
                'i.scale',
                'i.tags',
                'i.client',
                'i.ext',
                'i.description'
            )
            ->where('i.id = :id')
            ->setParameter('id', $id, PDO::PARAM_INT)
            ->getQuery()
            ->getOneOrNullResult();

        return $qb;
    }

    private function getIdUpdate(string $uuid, string $name): string
    {
        $qb = $this->createQueryBuilder('i')
            ->select(
                'i.id'
            )
            ->where('i.uuid = :uuid')
            ->andWhere('i.name = :name')
            ->setParameter('uuid', $uuid, PDO::PARAM_STR)
            ->setParameter('name', $name, PDO::PARAM_STR)
            ->getQuery()
            ->getOneOrNullResult();

        return (string) $qb['id'];
    }
}
