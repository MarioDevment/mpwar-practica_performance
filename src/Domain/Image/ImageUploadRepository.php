<?php
declare(strict_types = 1);

namespace MarioDevment\Performance\Domain\Image;

use MarioDevment\Performance\Domain\ValueObject\Uuid;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageEntry;

interface ImageUploadRepository
{
    public function addFile(Uuid $uuid): ImageEntry;
}
