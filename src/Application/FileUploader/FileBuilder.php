<?php

declare(strict_types = 1);

namespace MarioDevment\Performance\Application\FileUploader;

use MarioDevment\Performance\Domain\Image\ImageUploadRepository;
use MarioDevment\Performance\Domain\ValueObject\Uuid;
use MarioDevment\Performance\Infrastructure\Doctrine\Entity\Image\ImageEntry;

final class FileBuilder
{
    private $fileUpload;
    private $uuid;

    public function __construct(ImageUploadRepository $fileUpload, Uuid $uuid)
    {
        $this->fileUpload = $fileUpload;
        $this->uuid       = $uuid;
    }

    public function uploadImage(): ImageEntry
    {
        $file = $this->fileUpload->addFile($this->uuid);

        return $file;
    }
}
